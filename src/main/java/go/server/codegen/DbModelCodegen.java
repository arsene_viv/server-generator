package go.server.codegen;

import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import java.util.Properties;

import go.server.codegen.generators.ModelStructGenerator;
import go.server.codegen.util.DbColumn;
import go.server.codegen.util.DbUtils;

public class DbModelCodegen {
	
	public static void main(String[] args) throws Exception {

		if (2 > args.length) {
			System.out.println("Usage DbModelCodegen <output folder> <table1,table2,...>");
			System.exit(-1);
		}
		
		String outputFolder = args[0]; //"d:/gen2/go6/api2"
		String tables[] = args[1].split(","); //"card_goals", "cards", "events" 
		
		Properties props = new Properties();
		props.load(new FileReader(new File("db.properties")));
		Connection conn = DriverManager.getConnection(props.getProperty("url"), props);
		
		for (String table: tables) {
			List<DbColumn> columns = DbUtils.getColumns(conn, table);
			System.out.println("Generating beans for " + table + ", columns=" + columns);
			new ModelStructGenerator(outputFolder, table, columns).run(); //TODO run in executor
		}
		
		conn.close();
	}
}
