package go.server.codegen;

import java.util.Map.Entry;

import go.server.codegen.generators.HttpHandlerMethodGenerator;
import io.swagger.codegen.ClientOptInput;
import io.swagger.codegen.DefaultGenerator;
import io.swagger.codegen.config.CodegenConfigurator;
import io.swagger.models.Path;

public class RequestCodegen {
	
	public static void main(String[] args) {

		if (2 > args.length) {
			System.out.println("Usage: ClientCodegen <swagger file> <output folder>");
			System.exit(-1);
		}
		
		String spec = args[0];//"d:/eclipse_workspace/DbGen/swagger.yml";
		String output = args[1];//"d:/gen2/go6";

		String outputApiFolder = output + "/api2";
		
		CodegenConfigurator configurator = new CodegenConfigurator();

		configurator.setInputSpec(spec); // swagger file
		configurator.setLang("io.swagger.codegen.languages.GoServerCodegen"); // codegen class
		configurator.setOutputDir(output); // output folder
		
		final ClientOptInput clientOptInput = configurator.toClientOptInput();

		for (Entry<String, Path> path: clientOptInput.getSwagger().getPaths().entrySet()) {
			new HttpHandlerMethodGenerator(outputApiFolder, path).run();
		}
		DefaultGenerator dg = new DefaultGenerator();
		//dg.setGeneratorPropertyDefault(CodegenConstants.SUPPORTING_FILES, Boolean.FALSE.toString());
		dg.opts(clientOptInput);
		
		System.out.println("Swagger generated " + dg.generate());
	}

}
