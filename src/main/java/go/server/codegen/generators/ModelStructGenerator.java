package go.server.codegen.generators;

import static go.server.codegen.util.NameUtils.createFileName;
import static go.server.codegen.util.NameUtils.createName;
import static go.server.codegen.util.NameUtils.map;
import static go.server.codegen.util.SourceUtils.goFile;
import static go.server.codegen.util.SourceUtils.writeFile;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import go.server.codegen.ast.GoFile;
import go.server.codegen.ast.Struct;
import go.server.codegen.ast.StructElement;
import go.server.codegen.util.DbColumn;

public class ModelStructGenerator implements Runnable {

	private final String outputApiFolder;
	private final String tableName;
	private final List<DbColumn> columns;
	
	public ModelStructGenerator(String outputApiFolder, String tableName, List<DbColumn> columns) {
		super();
		this.outputApiFolder = outputApiFolder;
		this.tableName = tableName;
		this.columns = columns;
	}

	private String getType(String dbType, String typeName) {
		switch(dbType) {
			case "uuid":
				return "ID";
			case "jsonb":
				return "*" +  typeName + "Payload";
			default:
				return "TODO:" + dbType;
		}
	}

	private StructElement createElement(DbColumn col, String className) {
		List<String> fieldNameFragments = new ArrayList<>(Arrays.asList(col.getName().split("_")));
		return new StructElement(createName(fieldNameFragments), getType(col.getTypeName(), className), 
				map("db", col.getName()));
	}
	
	private Struct createStruct(String className, List<DbColumn> columns) {
		Struct struct = new Struct(className, new ArrayList<>());
		for (DbColumn col: columns)
			struct.getElements().add(createElement(col, className));
		return struct;
	}
	
	public void run() {
		List<String> nameFragments = new ArrayList<>(Arrays.asList(tableName.split("_")));
		String className = createName(nameFragments);
		String fileName = createFileName(nameFragments) + ".go";
		File targetFolder = new File(outputApiFolder + File.separator + "model");
		Struct struct = createStruct(className, columns);
		GoFile goFile = goFile("model", null, struct);
		writeFile(goFile, targetFolder, fileName); 
	}	

}
