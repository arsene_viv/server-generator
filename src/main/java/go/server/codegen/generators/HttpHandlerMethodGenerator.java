package go.server.codegen.generators;

import static go.server.codegen.util.NameUtils.createFileName;
import static go.server.codegen.util.NameUtils.createFragments;
import static go.server.codegen.util.NameUtils.createName;
import static go.server.codegen.util.SourceUtils.add;
import static go.server.codegen.util.SourceUtils.goFile;
import static go.server.codegen.util.SourceUtils.imports;
import static java.text.MessageFormat.format;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import go.server.codegen.ast.BlockStatement;
import go.server.codegen.ast.GoFile;
import go.server.codegen.ast.GoMethod;
import go.server.codegen.ast.Struct;
import go.server.codegen.ast.StructElement;
import go.server.codegen.util.SourceUtils;
import io.swagger.models.HttpMethod;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.parameters.PathParameter;

public class HttpHandlerMethodGenerator implements Runnable {

	private final String outputApiFolder;
	private final Entry<String, Path> path;

	public HttpHandlerMethodGenerator(String outputApiFolder, Entry<String, Path> path) {
		super();
		this.outputApiFolder = outputApiFolder;
		this.path = path;
	}

	private String getTypeForFormat(String f) {
		switch(f) {
		case "uuid":
			return "domain.ID";
		default:
			return "TODO: " + f;
		}
	} 

	private void printParamsStruct(String outputApiFolder, String path, Entry<HttpMethod, Operation> operation) {
		Operation op = operation.getValue();
		List<String> nameFragments = createFragments(path, operation.getKey());
		nameFragments.add("params");
		
		
		Struct struct = new Struct(createName(nameFragments), new ArrayList<>());
		struct.getElements().add(new StructElement("UserID", "uuid", null));
		for (io.swagger.models.parameters.Parameter p: op.getParameters()) {
			if (p instanceof PathParameter) {
				PathParameter pp = (PathParameter)p;
				struct.getElements().add(new StructElement(p.getName(), getTypeForFormat(pp.getFormat()), null));
			} else {
				struct.getElements().add(new StructElement("TODO", " supoport " + p.getClass(), null));
			}
		}
		
		GoFile goFile = goFile("apiparams", imports("bitbucket.org/vivhealth/viv-prototype/domain"), struct); 
	}	
	
	private void addIDParam(BlockStatement bs, String name, String method) {
		add(bs, format("{0}, ok := domain.ParseID({1})", name, method));
		BlockStatement ifError = new BlockStatement("if !ok", new ArrayList<>());
		bs.getStatements().add(ifError);
		add(ifError, 
			"responseBadRequest(w, fmt.Sprintf(\"" + name + " must be valid UUID: %q\", " + method + "))",
			"return");
		add(bs, format("req.{0} = {0}", name));
	}
	
	
	private void printHandler(String outputApiFolder, String path, Entry<HttpMethod, Operation> operation) {
			
		List<String> nameFragments = createFragments(path, operation.getKey());
		
		String methodName = createName(nameFragments);
		String fileName = createFileName(nameFragments);
		String paramsStruct = methodName + "Params";
		
		printParamsStruct(outputApiFolder, path, operation);
		
		Operation op = operation.getValue();

		GoFile gf = goFile("httphandler", 
			imports("fmt", "net/http", "bitbucket.org/vivhealth/viv-prototype/api",
					"bitbucket.org/vivhealth/viv-prototype/domain", "github.com/gorilla/mux"));
		
		GoMethod method = new GoMethod(
				"func (s *Service) " + methodName + "(w http.ResponseWriter, r *http.Request)",
				new ArrayList<>());
		gf.getRootSectionElements().add(method);
		
		BlockStatement bs = new BlockStatement("if token := getToken(w, r); token != nil", new ArrayList<>());
		method.getBody().add(bs);
		add(bs, 
			"req := &api." + paramsStruct + "{}",
			"vars := mux.Vars(r)");
		
		addIDParam(bs, "userID", "getUserID(token)");

		for (io.swagger.models.parameters.Parameter p: op.getParameters()) {
			if (p instanceof PathParameter) {
				String pName = p.getName();
				addIDParam(bs, pName, "domain.ParseID(vars[\"" + pName + "\"]");
			} else {
				add(bs, "TODO support " + p.getClass());
			}
		}
			
		add(bs, format("res, hErr := s.Service.{0}(r.Context(), req)", methodName));
		BlockStatement bs2 = new BlockStatement("if hErr != nil", new ArrayList<>());
		bs.getStatements().add(bs2);
		add(bs2, "responseError(w, hErr, \"\")", "return");
	} 

	public void run() {

		System.out.println("!!! processing " + path.getKey());
		System.out.println(path.getValue().getOperationMap());
		
		for (Entry<HttpMethod, Operation> operation: path.getValue().getOperationMap().entrySet()) {
			printHandler(outputApiFolder, path.getKey(), operation);
		}
	}

	
}
