package go.server.codegen.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

public class DbUtils {
	
	static List<DbColumn> getColumns(ResultSetMetaData rsmd) throws Exception {
		List<DbColumn> result = new ArrayList<>();
		for (int i=1; i <= rsmd.getColumnCount(); i++) {
			result.add(new DbColumn(rsmd.getColumnName(i), 
					rsmd.getColumnTypeName(i), 
					rsmd.getColumnType(i),
					rsmd.getColumnClassName(i)));
		}
		return result;
	}
	
	public static List<DbColumn> getColumns(Connection conn, String tableName) throws Exception {
		//System.out.println("---------- " + tableName +  " :begin");
		PreparedStatement st = conn.prepareStatement("select * from " + tableName);
		ResultSet rs = st.executeQuery();
		List<DbColumn> columns = getColumns(rs.getMetaData());
		rs.close();
		st.close();
		return columns;
	}
}