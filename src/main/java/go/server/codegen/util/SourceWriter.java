package go.server.codegen.util;

import java.io.PrintWriter;

public class SourceWriter {

	final PrintWriter pw;

	String indentInc = "    ";

	String indent = "";

	public void incIndent() {
		indent += indentInc;
	}
	
	public void decIndent() {
		indent = indent.substring(0, indent.length() - indentInc.length());
	}

	public void println(String line) {
		pw.print(indent);
		pw.println(line);
	}
	
	public void println() {
		pw.println();
	}

	public void println(Iterable<String> lines) {
		for (String line: lines) {
			pw.print(indent);
			pw.println(line);
		}
	}

	public void begin(String open) {
		pw.print(indent);
		pw.println(open);
		incIndent();
	}
	
	public void end(String close) {
		decIndent();
		pw.print(indent);
		pw.println(close);
	}

	public void printComment(String comment) {
		pw.print(indent);
		pw.print("// ");
		pw.println(comment);
	}
 	
 	public SourceWriter(PrintWriter pw) {
		super();
		this.pw = pw;
	}
}