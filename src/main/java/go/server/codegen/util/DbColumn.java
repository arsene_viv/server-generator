package go.server.codegen.util;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DbColumn {

	private final String name;
	private final String typeName;
	private final int typeCode;
	private final String className;
}