package go.server.codegen.util;

import static java.util.Arrays.asList;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import go.server.codegen.ast.BlockStatement;
import go.server.codegen.ast.GoFile;
import go.server.codegen.ast.GoFileWriter;
import go.server.codegen.ast.ImportSection;
import go.server.codegen.ast.SingleStatement;
import go.server.codegen.ast.StructOrMethod;

public class SourceUtils {

	public static GoFile goFile(String packageName, ImportSection imports, StructOrMethod...structOrMethods) {
		return 
			GoFile.builder().packageName(packageName)
			.sections(new ArrayList<>(asList(imports)))
			.rootSectionElements(new ArrayList<>(asList(structOrMethods))).build();
	}
	
	public static void add(BlockStatement bs, String...lines) {
		for (String line: lines)
			bs.getStatements().add(new SingleStatement(line));
	}
	
	public static void writeFile(GoFile f, File targetFolder, String fileName) {
		targetFolder.mkdirs();
		try (PrintWriter pw = new PrintWriter(new FileWriter(new File(targetFolder, fileName)))) {
			new GoFileWriter(new SourceWriter(pw)).writeFile(f);
		} catch(IOException ex) {
			ex.printStackTrace();
		}
	}

	public static ImportSection imports(String...lines) {
		return ImportSection.builder().imports(new ArrayList<>(asList(lines))).build();
	}
	
}
