package go.server.codegen.util;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.concat;
import static java.util.stream.Stream.of;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.swagger.models.HttpMethod;

public class NameUtils {

	public static String capitalize(String s) {
		return
			(null != s && 1 < s.length()) ? 
			s.substring(0, 1).toUpperCase() +  s.substring(1).toLowerCase() : s.toUpperCase();
	}
	
	public static String capitalize1(String s) {
		return
			(null != s && 1 < s.length()) ? 
			s.substring(0, 1).toUpperCase() +  s.substring(1) : s.toUpperCase();
	}
	
	public static boolean isNotPathParam(String f) {
		return f.length() > 0 && !(f.startsWith("{") && f.endsWith("}"));
	}
	
	public static List<String> createFragments(String path, HttpMethod method) {
		return concat(of(method.name()), stream(path.split("/")))
				.filter(NameUtils::isNotPathParam).collect(toList());
	}

	public static String createFileName(List<String> fragments) {
		return fragments.stream().map(String::toLowerCase).collect(joining("_"));
	}
	

	public static String createName(List<String> fragments) {
		return fragments.stream().map(NameUtils::capitalize).collect(joining(""));
	}
	
	public static Map<String, String> map(String...values) {
		Map<String, String> res = new LinkedHashMap<>();
		for (int i=0; i < values.length - 1; i += 2) {
			res.put(values[i], values[i+1]);
		}
		return res;
	}
}