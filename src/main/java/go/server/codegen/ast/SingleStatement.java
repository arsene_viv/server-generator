package go.server.codegen.ast;

import lombok.Data;

@Data
public class SingleStatement implements GoStatement {

	private final String line;
	
}
