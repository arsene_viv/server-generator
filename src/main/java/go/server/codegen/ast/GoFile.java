package go.server.codegen.ast;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GoFile {

	private final String packageName;
	
	private final List<FileSection> sections;
	 
	private final List<StructOrMethod> rootSectionElements;
}
