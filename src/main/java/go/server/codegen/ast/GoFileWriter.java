package go.server.codegen.ast;

import static java.util.stream.Collectors.joining;

import java.util.Map;

import go.server.codegen.util.SourceWriter;

public class GoFileWriter {
	
	private final SourceWriter sw;
	
	public GoFileWriter(SourceWriter sw) {
		super();
		this.sw = sw;
	}
	
	private void writeSingleStatement(SingleStatement st) {
		sw.println(st.getLine());
	}
	
	private void writeBlockStatement(BlockStatement bs) {
		sw.begin(bs.getPrefix() + " {");
		bs.getStatements().forEach(this::writeStatement);
		sw.end("}");
	}
	
	private void writeStatement(GoStatement st) {
		if (st instanceof SingleStatement) writeSingleStatement((SingleStatement)st);
		else if (st instanceof BlockStatement) writeBlockStatement((BlockStatement)st);
		else throw new UnsupportedOperationException();
	}
	
	private void writeMethod(GoMethod gm) {
		sw.begin(gm.getHeader() + " {");
		gm.getBody().forEach(this::writeStatement);
		sw.end("}");
	}

	private String printTags(Map<String, String> tags) {
		if (null == tags || tags.isEmpty()) return null;
		return tags.entrySet().stream().map(e -> e.getKey() + ":\"" + e.getValue() + "\"").collect(joining(","));
	}
	
	private void printStructElement(StructElement se) {
		StringBuilder sb = new StringBuilder();
		sb.append(se.getName()).append(" ").append(se.getType());
		String tags = printTags(se.getTags());
		if (null != tags) sb.append(" `").append(tags).append("`");
		sw.println(sb.toString());
	}
	
	private void writeStruct(Struct gs, boolean inTypeBlock) {
		sw.begin((inTypeBlock ? "" : "type ") + "struct {");
		if (null != gs.getElements()) {
			gs.getElements().forEach(this::printStructElement);
		}
		sw.end("}");
	}

	private void writeStructOrMethod(StructOrMethod sm) {
		if (sm instanceof Struct) writeStruct((Struct)sm, false);
		else if (sm instanceof GoMethod) writeMethod((GoMethod)sm);
		else throw new UnsupportedOperationException();
	}
	
	private void writeImportSection(ImportSection fs) {
		sw.begin("import (");
		sw.println(fs.getImports());
		sw.end(")");
	}
	
	private void writeTypeSection(TypeSection ts) {
		sw.begin("type (");
		for (Struct s: ts.getStructs()) {
			writeStruct(s, true);
		}
		sw.end(")");
	}
	
	private void writeSection(FileSection fs) {
		if (fs instanceof ImportSection) {
			writeImportSection((ImportSection)fs);
		} else if (fs instanceof TypeSection) {
			writeTypeSection((TypeSection)fs);
		} else throw new UnsupportedOperationException();
	}
	
	public void writeFile(GoFile goFile) {
		sw.println("package " + goFile.getPackageName());
		sw.println();
		if (null != goFile.getSections())
			goFile.getSections().stream().forEach(this::writeSection);
		if (null != goFile.getRootSectionElements()) 
			goFile.getRootSectionElements().forEach(this::writeStructOrMethod);
		
	}
	
}
