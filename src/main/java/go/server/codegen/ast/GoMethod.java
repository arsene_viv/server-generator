package go.server.codegen.ast;

import java.util.List;

import lombok.Data;

@Data
public class GoMethod implements StructOrMethod {

	private final String header;
	
	private final List<GoStatement> body;
}