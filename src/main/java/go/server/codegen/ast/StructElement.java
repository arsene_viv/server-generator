package go.server.codegen.ast;

import java.util.Map;

import lombok.Data;

@Data
public class StructElement {

	private final String name;
	
	private final String type;
	
	private final Map<String, String> tags;
	
}
