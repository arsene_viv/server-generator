package go.server.codegen.ast;

import java.util.List;

import lombok.Data;

@Data
public class Struct implements StructOrMethod {

	private final String name;

	private final List<StructElement> elements;
	
}
