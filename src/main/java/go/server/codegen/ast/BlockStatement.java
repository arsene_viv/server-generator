package go.server.codegen.ast;

import java.util.List;

import lombok.Data;

@Data
public class BlockStatement implements GoStatement {

	private final String prefix;
	
	private final List<GoStatement> statements;
	
}
