package go.server.codegen.ast;

import java.util.List;

import lombok.Data;

@Data
public class TypeSection implements FileSection {

	private List<Struct> structs;
}
