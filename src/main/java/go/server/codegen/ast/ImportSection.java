package go.server.codegen.ast;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ImportSection implements FileSection {

	private final List<String> imports;
	
}
